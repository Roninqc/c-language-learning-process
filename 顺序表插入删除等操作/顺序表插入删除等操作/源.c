#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#define Listsize 100  //顺序表可容纳最大值

//声明顺序表
typedef struct sqlist {
    int data[Listsize];  //存储顺序表中的元素
    int length;  //顺序表中含有的元素个数
};

//在顺序表中插入元素
void InsertList(struct sqlist* list, int t, int i) {  //插入位置为i,插入值为t
    int j;
    if (i<0 || i>list->length) {  //插入位置不合法
        printf("位置错误！");
        exit(1);
    }
    if (list->length >= Listsize) {  //超出顺序表范围，溢出
        printf("溢出");
        exit(1);
    }
    for (j = list->length - 1; j >= i; --j) {  //腾出位置i以供插入数据t
        list->data[j + 1] = list->data[j];
    }
    list->data[i] = t;  //在位置i插入数据t
    list->length++;  //顺序表长度加1
}

int main1()
{
    struct sqlist* sq;  //创建顺序表sq
    int i, n, t;
    sq = (struct sqlist*)malloc(sizeof(struct sqlist));  //分配空间
    sq->length = 0;  //初始化顺序表长度为0
    printf("输入顺序表的大小：");
    scanf("%d", &n);
    printf("请输入顺序表的元素：\n");  //在顺序表中插入n个元素
    for (i = 0; i < n; ++i) {
        scanf("%d", &t);
        InsertList(sq, t, i);  //插入位置为i，值为t
    }
    printf("这个链表现在是：\n");
    for (i = 0; i < sq->length; ++i) {  //输出顺序表的内容
        printf("%d ", sq->data[i]);
    }
    return 0;
}