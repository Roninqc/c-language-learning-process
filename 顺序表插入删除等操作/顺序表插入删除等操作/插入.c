#define _CRT_SECURE_NO_WARNINGS 1
#include "do.h"
#define Listsize 100

typedef struct {
	int data[Listsize];
	int length;
}Sqlist;
void InitList(Sqlist *L)
{//动态数组的初始化
	L->length = 0;
}
bool ListInsert(Sqlist *L, int e, int i)
{
	if ((i < 1) || i > L->length + 1|| L->length > Listsize)
	{
		return false;
	}
	int j=0;
	for (j = L->length - 1; j >= i-1; j--)
	{
		L->data[j] = L->data[j - 1];
	}
	L->data[i - 1] = e;
	L->length++;
	return true;
}
bool ListDelete(Sqlist *L, int i, int e)
{
	if ((i < 1) || i > L->length)
	{
		return false;
	}
	if (L->length > Listsize)
	{
		return false;
	}
	int j = 0;
	for (j = i; j < L->length; j++)
	{
		L->data[j - 1] = L->data[j];
	}
	L->length--;
	return true;
}