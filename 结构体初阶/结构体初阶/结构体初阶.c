#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
struct stu
{
	char name[20];
	int age;
	char id[20];
};
void print1(struct stu t)
{
	printf("%c %d %c", t.name, t.age, t.id);
}
void print2(struct stu* t)
{
	printf("%c %d %c", t->name, t->age, t->id);
}
int main()
{
	struct stu s={'张三',20,"202005034"};
	/*struct stu* ps = &s;
	printf("%c\n", (*ps).age);
	printf("%c\n", ps->age);*/
	print1(s);
	print2(&s);
	//当内存开销过大的时候最好用取地址的方式防止压栈
	return 0;
}