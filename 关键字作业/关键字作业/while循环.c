#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main12()
{
	int i = 1;
    while (i <= 10)
    {
		if (i == 5)
			continue;//跳过后面的代码，处理不当容易进入死循环
		printf("%d", i);
		i++;
	}
	return 0;
}
int main()
{
	int ch = 0;
	while ((ch = getchar())!=EOF)
	{
		putchar(ch);
	}//无限循环但是按ctrl+z可以停止循环
	return 0;
}