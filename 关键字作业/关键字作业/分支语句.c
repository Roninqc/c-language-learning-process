#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main06()
{
	int age = 60;
	if (age < 18)
		printf("少年");
	else if (age >= 18 && age < 30)//此时需要用并运算符来表示，否则则可能会导致程序出错。
		printf("青年");
	else if (age >= 30 && age < 50)
		printf("中年");
	else
		printf("老年");
	return 0;
}
int main07()
{
	int a = 0;
	int b = 2;
	if (a == 1)
		if (b == 2)
			printf("hh");
		else//else跟最近的if匹配，所以此时由于a不等于1所以完全无法执行，故输出为空
			printf("hehe");
	return 0;
}
int main08()
{
	int m = 0;
	scanf("%d", &m);
	if (m % 2 == 1)
		printf("奇数");
	else
		printf("偶数");
}
int main09()
{
	int m=1;
	while (m >= 1 && m <= 100)
	{
		if (m % 2 == 1)
			printf("%d\n",m);
		m++;
	}
	return 0;
}