#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main01()
{
	char arr[] = { 'a','b','c' };
	printf("%d\n", strlen(arr));
	return 0;
}
int main02()
{
	int a=0 ;
	int b=0 ;
	int sum ;
	scanf("%d %d", &a, &b);
	sum = a > b ? a : b;
	printf("%d\n", sum);
	return 0;
}
int Max(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}
int main03()
{
	int m;
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	m=Max(a, b);
	printf("%d\n", m);
	return 0;
}
int main04()
{
	printf("     **\n");
	printf("     **\n");
	printf("************\n");
	printf("************\n");
	printf("    *  *\n");
	printf("    *  *\n");
	return 0;
}
int main05()
{
	int m = 0;
	scanf("%d",&m);
	if (m % 5 == 0)
		printf("YES\n");
	else
		printf("NO\n");
	return 0;

}