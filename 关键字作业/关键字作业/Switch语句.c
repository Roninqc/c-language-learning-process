#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main10()
{
	int day;
	scanf("%d", &day);
	switch (day)
	{
	case 1:
		printf("星期一");
		break;//如果没有break语句则可能导致输入的值的同时输出该值后面的所有内容
	case 2:
		printf("星期二");
		break;
	case 3:
		printf("星期三");
		break;
	case 4:
		printf("星期四");
		break;
	case 5:
		printf("星期五");
		break;
	case 6:
		printf("星期六");
		break;
	case 7:
		printf("星期七");
		break;
	default:
		printf("输入错误");
		break;//在一般情况下可以一个后面加一个break。
	}
	return 0;
}
