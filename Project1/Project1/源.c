#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void init_board(char mine[ROWS][COLS], char set, int row, int col)//初始化棋盘函数为set
{
	memset(mine, set, row * col * sizeof(mine[0][0])); //memset本身就有初始化功能 括号内容依次为：
}	                                              //数组名，初始化目标，总共初始化字节

void set_mine(char mine[ROWS][COLS])//随机布雷函数
{
	int count = MAX;//设置计数器，记录要布入雷的个数
	while (count > 0)
	{
		int x = rand() % 9 + 1;//产生1到9的随机数字
		int y = rand() % 9 + 1;
		if ((mine[x][y]) == '0') //判断同一位置是否重复布雷
		{
			mine[x][y] = '1';
			count--; //每成功布雷一颗，计数器就减一
		}
	}
}

void display(char mine[ROWS][COLS], int row, int col)//打印棋盘函数
{
	int i = 0;
	int j = 0;
	printf("  ");//两个空格，为了使对齐，一个留给列号，一个留给棋盘
	for (i = 1; i <= row - 2; i++)//打印列号
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row - 2; i++)//输出的是棋盘信息
	{
		printf("%d ", i);
		for (j = 1; j <= col - 2; j++)
		{
			printf("%c ", mine[i][j]);//
		}
		printf("\n");
	}
}

int get_mine_count(char mine[ROWS][COLS], int x, int y)//统计坐标周围雷数目的函数
{
	return mine[x - 1][y - 1]
		+ mine[x][y - 1] + mine[x + 1][y - 1]
		+ mine[x + 1][y] + mine[x + 1][y + 1]
		+ mine[x][y + 1] + mine[x - 1][y + 1]
		+ mine[x - 1][y] - 8 * '0';  //由于数组中存放的是字符'0',周围是8个数字，所以-8*'0'才能返回雷数的int值
}
