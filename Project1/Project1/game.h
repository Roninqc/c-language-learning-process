#pragma once
#ifndef __GAME_H__
#define __GAME_H__

#define COLS 11 //列数
#define ROWS 11 //行数
#define COL (COLS-2)
#define ROW (ROWS-2) 
#define MAX 10 //雷的个数

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>//memset头文件

void init_board(char mine[ROWS][COLS], int row, int col);//声明初始化棋盘函数，在game.c实现
void set_mine(char mine[ROWS][COLS]); //声明布雷函数
void display(char mine[ROWS][COLS], int row, int col);//声明打印棋盘函数
int get_mine_count(char mine[ROWS][COLS], int x, int y);//声明统计坐标周围雷的数目函数

#endif//__GAME_H__
