#define _CRT_SECURE_NO_WARNINGS 1


#include"game.h"

void menu()
{
	printf("*********************************\n");
	printf("*********1.play 0.exit***********\n");
	printf("*********************************\n");
}

enum Option //枚举，增加代码可读性
{
	EXIT,  //默认为0
	PLAY   //为1
};

void game()
{
	char mine[ROWS][COLS] = { 0 };//非雷的位置可初始化为零
	char show[ROWS][COLS] = { 0 };//创建一个和mine一样大的数组来存放，坐标周围雷的数目
	int x = 0;
	int y = 0;
	int win = 0;
	init_board(mine, '0', ROWS, COLS);//初始化数组,各位置放零
	init_board(show, '*', ROWS, COLS);//初始化数组，各位置放*

	set_mine(mine);//布雷
	display(show, ROWS, COLS);//打印棋盘
	while (win < (ROW * COL - MAX)) //非雷的个数
	{
		printf("请输入排查坐标：>");
		scanf("%d%d", &x, &y);
		if (((x >= 1) && (x <= ROWS - 2)) && ((y >= 1) && (y <= COLS - 2)))//判断输入坐标是否在棋盘内
		{
			if (mine[x][y] == '1')
			{
				printf("很不幸，踩到地雷\n");
				break;
			}
			else    //每进入一次意味着排完一颗雷
			{

				int count = get_mine_count(mine, x, y);//通过此函数统计指定坐标周围有几个雷
				show[x][y] = count + '0';//为了打印*，所以把display函数打印的类型改为%c打印出来的是字符。-
				win++;
				display(show, ROWS, COLS);//-为了把数字也能打印出来，所以+'0',否则打印的是数字作为的ASCLL码的值
			}
		}
		else
		{
			printf("坐标输入有误\n");

		}
	}
	if (win == ROW * COL - MAX)
	{
		printf("恭喜你，排雷成功\n");
	}
	printf("雷阵如下\n");
	display(mine, ROWS, COLS);//为了在游戏结束后显示雷阵
}

void test()//为使主函数清洁，将使打印菜单指令置于此
{
	int input = 0;
	srand((unsigned int)time(NULL));//unsigned作为返回值的强制类型转换，如果NULL处不是空指针
	//而是某个地址，会将返回值所携带的信息往所指向内容中存一份
	do
	{
		menu();
		printf("请选择：\n");
		scanf("%d", &input);
		switch (input)
		{
		case PLAY:
			game();
			break;
		case EXIT:
			break;
		default:
			printf("选择错误，请重新选择!\n");
			break;
		}
	} while (input);
}

int main()
{
	test();

	return 0;
}
