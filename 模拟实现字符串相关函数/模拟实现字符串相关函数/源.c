#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<assert.h>
//void my_sy(char* Dest, char* src)
//{
//	assert(src != NULL);//断言找出错误
//	//当输入的值不能被改变时可以根据情况加上const
//	while (*Dest++ = *src)
//	{
//		;
//	}
//}
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxxx";
//	char arr2[] = "hello";
//	strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}
int my_strelen(const char* arr)
{
	assert(arr != NULL);
	int count = 0;
	while (*arr != '\0')
	{
		count++;
	}
	return count;
}
int main()
{
	const int num = 10;
	const int*  p = &num;
	p = &num;
	//const如果放在*左边，修饰的是*p表示指针指向的内容，是不能通过指针来
	//改变的，但是指针变量本身是可以修改的
	// 在右边的时候不能改变指针则下面上一个正确下一个错误（int* const p）
	//*p=20错误
	//p=&n正确
	printf("%d\n", num);
	return 0;
}