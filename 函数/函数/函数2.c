#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
int mex(int x,int y)
{
	return (x > y) ? x : y;
}
int main4()
{
	int x = 20;
	int y = 10;
	int max = 0;
	max = mex(x, y);
	printf("%d\n", max);
	return 0;
}
void swap(int x, int y)
{
	int z = 0;
	z=x;
	x = y;
	y = z;
	printf("交换后值为：%d  %d", x, y);//相当于函数用完后销毁
}
int main5()
{
	int x = 40;
	int y = 70;
	swap(x, y);
	return 0;
}
void swap2(int* pa, int* pb)//改变外面定义变量的值时候需要用指针，否则改变的只
//是定义内的值
{
	int z = 0;
	z = *pa;
	*pa = *pb;
	*pb = z;
}
int main6()
{
	int a = 10;
	int b = 20;
	swap2(&a, &b);
	printf("%d  %d", a, b);
	return 0;
}