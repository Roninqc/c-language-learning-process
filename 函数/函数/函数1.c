#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
int main1()
{
	char c;
	for (c = 'A'; c <= 'Z'; c++)
	{
		putchar(c);
	}
	return 0;
}
int main2()
{
	char arr1[20] = { 0 };
	char arr2[] = "hello bit";
	strcpy(arr1, arr2);//注意里面参数要跟初始化数据类型一致
	printf("%s\n", arr1);
	return 0;
}
int main3()
{
	char arr[] = "hello bit";
    memset(arr, 'x', 5);//第一个是需要设置的数组名，第二个是改变的值但是需要是
	//int类型，第三个是设置的值的个数。
	printf("%s\n", arr);
	return 0;
}