#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <math.h>
int main1()
{
	char arr5[] = "bit";
	char arr6[] = { 'b','i','t' };
	printf("%s\n", arr5);
	printf("%s\n", arr6);//由于第二个字符中没有出现/0这个结束标记所以会出现乱码
	printf("%d\n", strlen(arr5));
	printf("%d\n", strlen(arr6));//同上述原因，导致长度是一个随机值
	return 0;
}
int main2()
{
	int arr[10] = { 0 };//其中[]为下标引用操作符，在定义过程中不要用变量
	arr[4] = 5;
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (i = 0; i < sz; i++)
	{
		printf("%d\n", arr[i]);
	}
	return 0;

}