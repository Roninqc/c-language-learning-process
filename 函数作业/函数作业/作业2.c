#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <math.h>
int digsum123(int n)
{
	if (n > 9)
	{
		return digsum(n / 10) + n % 10;
	}
	else
	{
		return n;
	}
}
int main8()
{
	int num = 1729;
	int sum = digsum(num);
	printf("%d\n", sum);
	return 0;
}
double pow1(int n,int k)
{
	if (k > 0)
	{
		return n * pow1(n, k - 1);
	}
	else if(k==0)
	{
		return 1;
	}
	else
	{
		return 1.0 / pow1(n, -k);
	}

}
int main9()
{
	int n = 0;
	int k = 0;
	scanf("%d %d", &n, &k);
	double x = pow1(n,k);
	printf("%lf\n", x);
	return 0;
}
int fei(int n)
{
	if (n > 2)
	{
		return fei(n - 1) + fei(n - 2);
	}
	if (n <= 2)
	{
		return 1;
	}
}
int fib(int n)
{
	int x = 1;
	int y = 1;
	int z = 1;
	while (n > 2)
	{
		z = x + y;
		x = y;
		y = z;
		n--;
	}
	return z;
}
int main10()
{
	int n = 0;
	scanf("%d", &n);
	int ret = fib(n);
	printf("%d\n", ret);
	return 0;
}