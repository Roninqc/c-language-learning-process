#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <math.h>
int su(int x)
{
	int y = 0;
	for (y = 2; y < sqrt(x); y++)
	{
		if (x % y == 0)
		{
			return 0;
		}
	}
	return 1;
}
int main1()
{
	int x = 1;
	for (x = 100; x <= 200; x++)
	{
		if (su(x) == 1)
		{
			printf("是素数为%d\n",x);
		}
	}
}
int run(int n)
{
	if (n % 4 == 0 && n %100 != 0 || n % 400 == 0)
	{
		return 1;
	}
}
int main2()
{
	int n = 0;
	for (n = 1000; n < 2000; n++)
	{
		if (run(n) == 1)
		{
			printf("是闰年为：%d\n", n);
		}
	}
	return 0;
}
int swap(int* a, int* b)
{
	int temp = 0;
	temp = *a;
	*a = *b;
	*b = temp;
	return 0;
}
int main3()
{
	int a = 10;
	int b = 20;
	swap(&a, &b);
	printf("%d\n", a);
	printf("%d\n", b);
	return 0;
}
int cheng(int n)
{
	int x = 0;
	for (x = 1; x <= n; x++)
	{
		int j = 0;
		for (j = 1; j <= x; j++)
		{
			printf(" %d*%d= %d", x, j, x * j);
		}
		printf("\n");
	}
}
int main4()
{
	int n = 0;
	scanf("%d", &n);
	cheng(n);
	return 0;
}
int ying(int n)
{
	if (n > 9)
	{
		ying(n / 10);
	}
	printf("%d\n", n % 10);
}
int main5()
{
	int n = 0;
	scanf("%d", &n);
	ying(n);
	return 0;
}
int jie(int x)//使用递归的时候需要用if限定不然可能输出不了
{
	if (x > 1)
	{
		return x * jie(x - 1);
	}
}
int main6()
{
	int n = 5;
	int m = jie(n);
	printf("%d\n", m);
	return 0;
}
int my_strlen(char *n)
{
	if (*n == '\0')
	{
		return 0;
	}
	else
	{
		return 1 + my_strlen(n+1);
	}
}
void ni(char* str)
{
	int left = 0;
	int right = my_strlen(str) - 1;
	char temp = 0;
	while (left < right)
	{
		temp = str[left];
		str[left] = str[right];
		str[right] = temp;
		left++;
		right--;
	}
}
void reverse_string(char* str)
{
	char i = *str;
	int len = my_strlen(str);
	*str = *(str + len - 1);
	*(str + len - 1) = '\0';
	if(my_strlen(str+1)>=2)
	{
		reverse_string(str + 1);
	}
	*(str + len - 1) = i;
	
}
int main7()
{
	char arr[] = { "abcdef" };
	reverse_string(arr);
	printf("%s\n", arr);
	return 0;
}