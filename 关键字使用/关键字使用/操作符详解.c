#define _CRT_SECURE_NO_WARNINGS 1
#include "c.h"

int main123()
{
	double a = 3.0 / 5;//这样写才会显示出小数，否则无法。
	//而%操作符两端必须是整数。
	printf("%lf\n", a);
	int x = -1;
	int y = x << 1;//左移为-2右移动为-1
	//原码：100000000000000000000000001
	//反码：111111111111111111111111110
	//补码：111111111111111111111111111
	//此时存放的是补码，当右移一位的时候由于是符号位故原值不变
	printf("%d\n", y);

	return 0;
}
int main2()
{
	int a = 3;
	int b = 5;
	int c = a & b;
	//a|b(判断是否其中一个为1，如果满足则输出1)
	// a^b（判断两个数对应的值是否相同，相同取0否则取1）
	//将两个数转化为二进制数，然后根据是否都为一判断值
	//用抑或交换两个元素
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("%d %d\n", a, b);

}
int main12312()
{
	int x = 10;
	int y = 0;
	int count = 0;
	for (y = 0; y < 32;y++)
	{
		if ((x >> y) & 1 == 1)
		{
			count++;
		}
	}
	printf("%d\n", count);
	return 0;
}