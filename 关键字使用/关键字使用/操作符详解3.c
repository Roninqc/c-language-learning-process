#define _CRT_SECURE_NO_WARNINGS 1
#include "c.h"
int main121212121212()
{
	int a = 0;
	int* pa = &a;;//这里的*表明pa是一个指针变量
	*pa = &a;//这里是解引用操作符
}
void test1(int arr[])
{
	printf("%d\n", sizeof(arr));
}
void test2(char ch[])
{
	printf("%d\n", sizeof(ch));
}
int main333()
{
	int arr[10] = { 0 };
	char ch[10] = { 0 };
	printf("%d\n", sizeof(arr));//40
	printf("%d\n", sizeof(ch));//10
	test1(arr);//8此时传入的只是首地址，由于此编译器是64位编译，故指针大小为8
	test2(ch);//8
	return 0;
}
int main123122223()
{
	int i = 0, a = 0, b = 2, c = 3, d = 4;
	//i = a++ && ++b && d++;//此时如果由于a为0所以后面全部为假，故没有进行计算
	i = a++ || ++b || d++;//或运算符左边为真时后面则不发生算术。
	printf("a=%d\nb=%d\nc=%d\nd=%d\n", a, b, c, d);//1 2 3 4
	return 0;
}