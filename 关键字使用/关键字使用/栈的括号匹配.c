#define _CRT_SECURE_NO_WARNINGS 1
#include "c.h"
#define MAXSIZE 10
typedef struct {
	int data[MAXSIZE];
	int top;
}stack;
bool init(stack* s)
{
	s->top = -1;
	return true;
}
bool empty(stack* s)
{
	if (s->top == -1)
		return true;
	else
		return false;
}
bool push(stack* s, char* x)
{
	int top=-1;
	if (s->top == MAXSIZE - 1)
		return false;
	s->data[++top] = x;
	return true;
}
bool pop(stack* s, char* x)
{
	int top=MAXSIZE;
	if (s->top == -1)
		return false;
	x = s->data[top--];
	return true;
}
