#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main01()
{
	int a = 3;
	printf("%d\n", ~a);
	return 0;
}

int main02()
{
	int a = 10;
	int b = ++a;//先++后使用//后置++先使用后++

	printf("%d\n", b);
	printf("%d\n", a);
	return 0;

}

int main04()
{
	int a = (int)3.14;//强制类型转换
	printf("%d\n", a);
	return 0;
}
int main05()
{
	int a = 0;
	int b = 2;
	int max;

	max = a > b ? a : b;//三目运算符
	printf("%d\n", max);
	return 0;
}
int main06()
{
	int a = 0;
	int b = 3;
	int c = 5;
	int d = (a = b + 2, c = a - 4, b = c + 2);//逗号表达式输出的是最后一个公式输出的值，但是求值仍然是从左往右
	printf("%d\n", d);
	return 0;
}
