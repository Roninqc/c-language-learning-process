#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main07()
{
	register int num = 100;//将NUM的数值存放到寄存器之中
}
typedef unsigned int uint;//该关键字可以使得类型变得更简单
int main08()
{
	uint num = 100;
	return 0;
}

void text()
{
	static int a = 1;//static使得a不会被销毁所以此时仍为原来数值，改变了生命周期（本质改变了存储类型）本质上它把外部链接属性变成了内部，从而使得其他源文件中若被其修饰则无法声明。
	a++;
	printf("%d\n", a);
}
int main09()
{
	int i = 0;
	while (i < 10)
	{
		text();
		i++;
	}
	return 0;
}
#define ADD(X,Y) (X+Y)//define定义宏
int main10()
{
	printf("%d\n", 4 * ADD(2, 3));
	return 0;
}
