#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main11()
{
	int a = 10;
	printf("%p\n", &a);
	return 0;
}
int main12()
{
	int a = 10;
	int* pa = &a;//*说明pa是指针，变量。前面的int说明pa指向的对象是int类型
	return 0;
}
int main13()
{
	int a = 10;
	int* pa = &a;
	*pa = 20;//解引用操作，通过找到pa里面的地址来改变值并赋予。
	printf("%d\n", a);
	return 0;
}
//在当前编译器中各个类型指针大小都是四个字节