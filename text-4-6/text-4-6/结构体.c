#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main14()
{
	float a = 10;
	printf("%lf\n", a);
	return 0;
}
struct stu
{
	char name[20];
	int age;
	float score;
};
int main()
{
	struct stu s = { "张三", 20 ,85.5};
	printf("%s %d %lf", s.name, s.age, s.score);
	struct stu* pa = &s;
	printf("%s %d %lf", pa->name, pa->age, pa->score);//拿到结构体指针的时候可以直接结构体指针加该符号后直接可得指向
	return 0;
}