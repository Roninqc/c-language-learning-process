#define _CRT_SECURE_NO_WARNINGS 1
#include "c.h"
int main11222()
{
	int a = 10;
	int* pa = &a;
	*pa = 10;//32位机器上一个大小为4字节64位机器上一个大小为8字节
	int a = 0x11223344;
	char* pc = &a;
	*pc = 0;//int类型改变四个字节，char类型改变了一个字节。
	//上述指针类型决定了：指针解引用权限有多大（上面的类型为对此表示的示例）。
	//指针类型同时也决定了，指针走一步，能走多远
	int arr[10] = { 0 };
	char* p = arr;
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		*(p + i) = 1;//相当于是取了数组一个元素地址加入可以等价于赋值
	}
	//初始化的时候对于指针变量的初始化可以将其设为NULL
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	char c[5];
	printf("%d\n", &arr[9] - &c[0]);//出错
	return 0;
}
int my_strlen(char* str)//指向初始指针计数方法
{
	int count = 0;
	while (str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
int strlen__(char* str)//指针减法的计数方法
{
	char* start = str;
	while (*str != '\0')
	{
		str++;
	}
	return str - start;
}
