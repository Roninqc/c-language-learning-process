#define _CRT_SECURE_NO_WARNINGS 1
#include "c.h"
#define N_VALUES 5
int main()
{
	int* vp = NULL;
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	for (vp = &arr[N_VALUES - 1]; vp >= &arr[0]; vp--)
	{
		*vp = 0;
	}//在C标准中，这种写法是错误的，以下为正确用法。
	for (vp = &arr[N_VALUES]; vp >= &arr[0];)
	{
		* --vp = 0;
	}
	//数组名代表的是数组首元素的地址
	int* p = arr;
	printf("%d\n", 2[arr]);//表示的是arr中的第二个元素
	//arr[2]==*(arr+2)==*(p+2)==*(2+arr)==2[arr]
	int** ppa = &p;//查找的是存储P指针的地址
	int* arr1;//整型指针数组
	char* arr2;//字符型指针数组
	return 0;
}