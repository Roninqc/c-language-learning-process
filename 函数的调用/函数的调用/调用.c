#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
int a(int x)//函数调用的时候判断需要用到返回值。
{
	int j = 0;
	for (j = 2; j <= x - 1; j++)
	{
		if (x % j == 0)
		{
			return 0;
		}
	}
	return 1;
}
int main()
{
	int i = 0;
	for (i = 1; i <= 100;i++)
	{
		if (a(i) == 1)
		{
			printf("%d\n", i);
		}
	}
	return 0;
}
int b(int x)
{
	if (x % 4 == 0 && x % 100 != 0 || x % 400 == 0)
	{
		return 1;
	}
	return 0;
}
int main2()
{
	int n = 0;
	for (n = 1000; n <= 2000; n++)
	{
		if (b(n) == 1)
		{
			printf("是闰年%d\n", n);
		}

	}
	return 0;
}
int search(int a[],int k,int sz)//数组在函数定义时候传过来的实参只是第一个地址所以导致
//计算长度的时候导致返回值仅为1.
{
	int left = 0;
	int right = sz - 1;
	while (left<=right)
	{
		int mid = (right + left) / 2;
		if (a[mid] < k)
		{
			left = mid + 1;
		}
		else if (a[mid] > k)
		{
			right = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}
int main3()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int abc = search(arr, k,sz);
	if (-1 == abc)
	{
		printf("对不起，没有找到");
	}
	else
	{
		printf("找到了，下标是:%d", abc);
	}
	return 0;
}
void Add(int* p)
{
	(*p)++;
}
int main4()
{
	int num = 0;
	Add(&num);//如果要改变函数里面的形参，需要的是给定地址否则改变不了
	printf("%d\n", num);
	return 0;
}