#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <time.h>
#include <math.h>
int my_strlen(char *n)
{
	if (*n != '\0')
	{
		return 1 + my_strlen(n+1);
	}
	else
	{
		return 0;
	}
}
int main4()
{
	char arr[] = "bit";
	printf("%d\n", my_strlen(arr));
	return 0;
}
int jiecheng(int x)
{
	if (x <= 1)
	{
		return 1;
	}
	else
	{
		return x * jiecheng(x - 1);
	}
}
int main5()
{
	int n = 0;
	scanf("%d", &n);
	int c = jiecheng(n);
	printf("%d\n", c);
	return 0;
}
int fib(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		return fib(n - 1) + fib(n - 2);
	}
}
int main6()//效率太低容易导致程序时常算不出来结果，可以改成循环结构
{
	int x = 0;
	scanf("%d", &x);
	int u=fib(x);
	printf("%d\n", u);
	return 0;
}
int jie(int n)
{
	int a = 1;
	int b = 1;
	int c = 1;
	while (n > 2)
	{
		c = a + b;
		a = b;
		b = c;
		n--;
	}
	return c;
}
int main7()//效率很高，但是结果可能不正确因为有时候数字太大会导致栈溢出
{
	int x = 0;
	scanf("%d", &x);
	int u = jie(x);
	printf("%d\n", u);
	return 0;
}
int count = 0;
int han(int n)//汉诺塔问题
{
	if (n == 1)
	{
		count++;
		printf("%d\n", count);
	}
	else
	{
		han(n - 1);
		count++;
		han(n - 1);
	}
	return count;
}
int main12()
{
	int n,s;
	scanf("%d", &n);
	s=han(n);
	printf("%d\n", s);
	return 0;
}

void hanoi(int n, char a, char b, char c)
{
	if (n == 1)//判断结束标志
	{
		count++;//计数
		printf("%c->%c\n", a, c);
	}

	else
	{
		hanoi(n - 1, a, c, b);//递归函数，将a盘移到c盘上去。
		count++;//计数
		printf("%c->%c\n", a, c);//递归函数，将b盘移到a盘上去
		hanoi(n - 1, b, a, c);

	}

}
int main()
{
	int n;
	printf("请输入要移动的块数：");
	scanf("%d", &n);

	hanoi(n, 'a', 'b', 'c');

	printf("移动次数：%d\n", count);//输出移到的次数

	return 0;
}
int frog(int n)
{
	int x = 1;
	int y = 1;
	int z = 1;
	while (n > 2)
	{
		z = x + y;
		x = y;
		y = z;
		n--;
	}
	return z;
}
int main13()
{
	int x = 0;
	scanf("%d", &x);
	int o = frog(x);
	printf("%d\n", o);
	return 0;

}