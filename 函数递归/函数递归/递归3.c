#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <time.h>
#include <math.h>
int main14()
{
	int n = 0;
	int count = 0;
	for (n = 1; n <= 100; n++)
	{
		if (n % 10 == 9)
		{
			count++;
		}
		if (n / 10 == 9)
		{
			count++;
		}
	}
	printf("%d\n", count);
	return 0;
}

int main15()
{
	int n = 0;
	double sum = 0.0;
	for (n = 1; n <= 100; n++)
	{
		if (n % 2 == 0)
		{
			sum -= 1.0 / n;//由于计算机对于小数判别需要引入浮点型故由此定义
		}
		else
		{
			sum += 1.0 / n;
		}
	}
	printf("%lf\n", sum);
	return 0;
}
int main16()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	int max = 0;
	for (i = 0; i < 10; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}
	printf("%d\n", max);

	return 0;
}
int main17()
{
	int i = 0;
	int j = 0;
	for (i = 1; i <= 9; i++)
	{
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=*%d ", i, j, i * j);
		}
		printf("\n");
	}
	return 0;
}
int main18()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int left = 0;
	int right = sizeof(arr) / sizeof(arr[0]) - 1;
	int k = 7;
	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了为%d\n", mid);
			break;
		}
	}
	return 0;
}
int main19()
{
	char input[20] = { 0 };
	system("shutdown -s -t 60");
	printf("若输入我是猪则可以取消关机");
again:	scanf("%s", input);
	if (strcmp(input, "我是猪") == 0)
	{
		system("shutdown -a");
	}
	else
		goto again;
	return 0;
}
void menu()
{
	printf("************************************\n");
	printf("**********  1.play   ***************\n");
	printf("**********  2.exit   ***************\n");
	printf("************************************\n");
}
void guess()
{
	int n = 0;
	int x = rand() % 100 + 1;
	while (1)
	{
		scanf("%d", &n);
		if (n > x)
		{
			printf("太大\n");
		}
		else if (n < x)
		{
			printf("太小\n");
		}
		else
		{
			printf("恭喜你猜对了!\n");
			break;
		}
	}
}
int main20()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			guess();
			break;
		case 2:
			break;
		default :
			break;
		}
	} while (input);
	return 0;

}
int pan(int x)
{
	int j = 0;
	for (j = 2; j < x ; j++)
	{
		if (x % j == 0)
		{
			return 0;
		}
	}
	return 1;
}
int main123()
{
	int x = 0;
	for (x = 100; x <= 200; x++)
	{
		if (pan(x) == 1)
		{
			printf("%d\n",x);
		}
	}
	return 0;
}
