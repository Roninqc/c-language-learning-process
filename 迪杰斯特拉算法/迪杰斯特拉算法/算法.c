#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <Windows.h>
#define MaxvertexNum 100
#define INF 0x3f3f3f3f
typedef struct Graph {
	char Vex[MaxvertexNum];
	int edge[MaxvertexNum][MaxvertexNum];
	int vexnum, arcnum;
}Graph;
bool S[MaxvertexNum];
int pr[MaxvertexNum];
int D[MaxvertexNum];
void Dijkstra(Graph G, int v)
{
	int n = G.vexnum;
	for (int i = 0; i < n; i++)
	{
		S[i] = false;
		D[i] = G.edge[v][i];
		if (D[i] < INF)
			pr[i] = v;
		else
			pr[i] = -1;
	}
	S[v] = true;
	D[v] = 0;
	for (int i = 0; i < n; i++)
	{
		int temp;
		int w;
		int min = INF;
		for (w = 0; w < n; w++)
		{
			if (!S[w] && D[w] < min) {
				temp = w;
				min = D[w];
			}
			
		}
		S[w] = true;
		for (w = 0; w < n; w++)
		{
			if (!S[w] && D[temp] + G.edge[temp][w] < D[w])
			{
				D[w] = D[temp] + G.edge[temp][w];
				pr[w] = v;
			}
		}
	}
}