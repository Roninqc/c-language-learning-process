#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
void Initboard(char arr[ROWS][COLS], int rows, int cols, char set);
void print(char arr[ROW][COL], int row, int col);
void SetMine(char arr[ROW][COL], int row, int col);
void SearchMine(char mine[ROW][COL], char arr[ROW][COL], int row, int col);