#define _CRT_SECURE_NO_WARNINGS 1
#include "game2.h"
void menu()
{
	printf("*************************************\n");
	printf("*********** 1.play  *****************\n");
	printf("*********** 2.exit  *****************\n");
	printf("*************************************\n");
}
void game()
{
	//创建两个棋盘，一个用来存储无雷区，一个是排雷区
	char mine[ROWS][COLS]={0};
	char show[ROWS][COLS]={0};
	//初始化棋盘
	Initboard(mine, ROWS, COLS,'0');
	Initboard(show, ROWS, COLS,'*');
	//打印棋盘；
	print(show, ROW, COL);


	SetMine(mine, ROW, COL);

	SearchMine(mine, show, ROW, COL);
}
int main()
{
	int input;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			printf("开始游戏\n");
			game();
			break;
		case 2:
			printf("离开游戏\n");
			break;
		default:
			printf("输入的值不合法，请重新输入\n");
			break;
		}
	} while(input);
	return 0;
}