#define _CRT_SECURE_NO_WARNINGS 1
#include "game2.h"
void Initboard(char arr[ROWS][COLS], int rows, int cols,char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			arr[i][j] = set;
		}
	}
}
void print(char arr[ROWS][COLS], int row, int col)
{
	int i;
	int j;
	printf("----------------扫雷游戏------------------\n");
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
	printf("----------------扫雷游戏------------------");
	return 0;
}
void SetMine(char arr[ROW][COL], int row, int col)
{
	int set = 10;
	while (set)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (arr[x][y] == '0')
		{
			arr[x][y] = '1';
			set--;
		}
	}
}
static int getcount(char arr[ROWS][COLS],int x,int y)
{
	return arr[x - 1][y - 1]
		+ arr[x][y - 1] + arr[x + 1][y - 1]
		+ arr[x + 1][y] + arr[x + 1][y + 1]
		+ arr[x][y + 1] + arr[x - 1][y + 1]
		+ arr[x - 1][y] - 8 * '0';
}
void open_mine(char mine[ROWS][COLS], char show[ROWS][COLS],int x, int y)
//坐标周围展开函数
{
	if (mine[x - 1][y - 1] == '0')
	{
		show[x - 1][y - 1] = getcount(mine,x - 1, y - 1) + '0';
		//显示该坐标周围雷数
	}
	if (mine[x - 1][y] == '0')
	{
		show[x - 1][y] = getcount(mine,x - 1, y) + '0';//显示该坐标周围雷数
	}
	if (mine[x - 1][y + 1] == '0')
	{
		show[x - 1][y + 1] = getcount(mine,x - 1, y + 1) + '0';//显示该坐标周围雷数
	}
	if (mine[x][y - 1] == '0')
	{
		show[x][y - 1] = getcount(mine,x, y - 1) + '0';//显示该坐标周围雷数
	}
	if (mine[x][y + 1] == '0')
	{
		show[x][y + 1] = getcount(mine,x, y + 1) + '0';//显示该坐标周围雷数
	}
	if (mine[x + 1][y - 1] == '0')
	{
		show[x + 1][y - 1] = getcount(mine,x + 1, y - 1) + '0';//显示该坐标周围雷数
	}
	if (mine[x + 1][y] == '0')
	{
		show[x + 1][y] = getcount(mine,x + 1, y) + '0';//显示该坐标周围雷数
	}
	if (mine[x + 1][y + 1] == '0')
	{
		show[x + 1][y + 1] = getcount(mine,x + 1, y + 1) + '0';//显示该坐标周围雷数
	}
}
void SearchMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y=0;
	int win = 0;
	while (win<row*col-10)
	{
		printf("请输入排查坐标\n");
		scanf("%d%d", &x,&y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾你输了\n");
				print(mine, row, col);
				break;
			}
			else
			{
				int count = getcount(mine, x, y);
				show[x][y] = count + '0';
				win++;
				open_mine(mine, show, x, y);
				print(show, row, col);
			}
		}
		else
		{
			printf("输入不合法请重新输入\n");
		}
	}
	if (win == row * col - 10)
	{
		printf("恭喜你，排雷成功\n");
	}
}