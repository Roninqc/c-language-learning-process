#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#define Max 10000;
extern int g;
int main01(void)
{

	printf("%d\n", g);
	
	int x = Max;
	printf("%d\n", x);
	return 0;
	
}

//枚举常量
//可以一一列举的常量,不能够更改。打印输出时按照从上往下顺序从0 1 2开始打印
enum sex
{
	MALE,
	FEMALE,
	SECRET,
};
int main20(void)
{
	enum Sex s = MALE;
	printf("%d\n", MALE);
	printf("%d\n", FEMALE);
	printf("%d\n", SECRET);
	return 0;
}

int main(void)
{
	char arr1[] = { 'a', 'b', 'c' };
	char arr2[] = ("abc");

	printf("%d\n", strlen(arr1));
	printf("%d\n", strlen(arr2));
	return 0;
}