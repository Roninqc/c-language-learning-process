#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main07(void)
{
	printf("%s\n", "abc");
	printf("%s\n", "\"");
	printf( "c:\\test\\test.c");
	printf("\a");
	printf("%c\n", '\101');//表示八进制数对应的ACILL值
	printf("%c\n", '\x30');//表示十六进制数对应的ACILL值
	printf("%d\n", strlen("c:\test\328\test.c"));
	return 0;
}