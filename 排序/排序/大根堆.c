#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

void HeaAdjust(int A[], int k, int len)
{
	A[0] = A[k];
	for (int i = 2 * k; i <= len; i *= 2)
	{
		if (i < len && A[i] < A[i + 1])
		{
			i++;
		}
		if (A[0] >= A[i])
		{
			break;
		}
		else
		{
			A[k] = A[i];
			k = i;
		}

	}
	A[k] = A[0];
}
void BuildMaxHeap(int A[], int len)
{
	for (int i = len / 2; i > 0; i--)
	{
		HeaAdjust(A, i, len);
	}
}
void swap1(int* a, int* b)
{
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}
void Heapsort(int A[], int len)
{
	BuildMaxHeap(A, len);
	for (int i = len; i > 1; i--)
	{
		swap1(&A[i], &A[1]);
		HeaAdjust(A, 1, i - 1);
	}
}
int main()
{
	int A[] = { 0,53,17,78,9,45,65,87,32 };
	Heapsort(A, 9);
	return 0;
}