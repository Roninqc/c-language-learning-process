#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
int Partition(int A[], int low, int high)
{
	int privot = A[low];
	while (low < high)
	{
		while (low < high && A[high] >= privot)
		{
			--high;
		}
		A[low] = A[high];
		while (low < high && A[low] <= privot)
		{
			++low;
		}
		A[high] = A[low];
	}
	A[low] = privot;
	return low;
}
void Quicksort(int A[], int low, int high)
{
	int privvot = Partition(A, low, high);
	Quicksort(A, low, privvot - 1);
	Quicksort(A, privvot + 1, high);
}