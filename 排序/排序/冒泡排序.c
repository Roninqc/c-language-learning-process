#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
int bubble(int A[], int n)
{
	int i, j, temp;
	for (i = 0; i < n - 1; i++)
	{
		bool flag = false;
		for (j = n - 1; j > i; j--)
		{
			if (A[j - 1] > A[j])
			{
				temp = A[j-1];
				A[j - 1] = A[j];
				A[j] = temp;
				flag = true;
			}
			if (flag == false)
			{
				return 0;
			}
		}
	}
}