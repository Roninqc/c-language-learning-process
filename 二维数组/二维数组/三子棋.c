#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "game.h"
void menu()
{
	printf("*************************************\n");
	printf("*********** 1.play  *****************\n");
	printf("*********** 2.exit  *****************\n");
	printf("*************************************\n");
}
void game()
{
	char arr[ROW][COL];
	//初始化棋盘
	chushi(arr, ROW, COL);
	//打印棋盘
	board(arr, ROW, COL);
	char ret=0;
	while (1)
	{
		Playrmove(arr, ROW, COL);
		board(arr, ROW, COL);
		ret = win(arr, ROW, COL);
		if (ret != 'C')
			break;
		Computermove(arr, ROW, COL);
		board(arr, ROW, COL);
		ret = win(arr, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '#')
	{
		printf("玩家赢\n");
	}
	else if (ret == '*')
	{
		printf("电脑赢\n");
	}
	else
	{
		printf("平局\n");
	}
	board(arr, ROW, COL);
}

int main()
{
	int input;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			printf("开始游戏:\n");
			game();
			break;
		case 2:
			printf("结束游戏");
			break;
		default:
			printf("输入错误，请重新输入");
			break;
		}
	} while (input);
	return 0;
}