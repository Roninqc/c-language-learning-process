#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <math.h>
//数组名是数组首元素地址
//但是有两个例外
//1.sizeof（数组名）-数组名表示整个数组-计算的是整个数组大小，单位字节
//2.&数组名-数组名表示整个数组-取出的是整个数组的地址。

void bubblesort(int arr[],int sz)
{
	//数组传参的时候传递的是首地址
	//所以不能在函数调用中使用此方法计算。
	int j = 0;
	for (j = 0; j < sz - 1; j++)
	{
		int i = 0;
		for (i = 0; i < sz - 1 - j; i++)
		{
			if (arr[i] > arr[i + 1])
			{
				int tmp = arr[i];
				arr[i] = arr[i+1];
				arr[i + 1] = tmp;
			}
		}
	}
}
int main123()
{
	int k = 0;
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubblesort(arr,sz);
	for (k = 0; k < sz; k++)//数组打印需要注意的是用遍历然后一个个输出
	{
		printf("%d ", arr[k]);
	}
	return 0;
}
void bubble(int arr[], int sz)
{
	int i;
	for (i = 0; i < sz - 1; i++)
	{
		int j;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j + 1] < arr[j])
			{
				int tem = arr[j];
				arr[j] = arr[j+1];
				arr[j + 1] = tem;
				
			}
		}
	}
}
int search(int arr[],int sz,int x)
{
	int left = 0;
	int right = sz - 1;
	int i = 0;
	while (left < right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] < arr[x])
		{
			left = mid + 1;
		}
		else if (arr[mid] > arr[x])
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标为：%d", mid);
		}
	}

}
void pai(int arr[],int x)
{
	int j,i;
	for (i = 0; i < x; i++)
	{
		for (j = i + 1; j < x; j++)
		{
			if (arr[i] > arr[j])
			{
				int tem = arr[i];
				arr[i] = arr[j];
				arr[j] = tem;
			}
		}
	}
}
int main13()
{
	int i = 0;
	int arr[9] = { 1,2,3,9,5,6,4,8 };
	pai(arr,8);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", arr[i]);
	}
}