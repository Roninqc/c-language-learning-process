#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <math.h>
int main4()
{
	//int arr[3][4] = { 1,2,3,4,5,6,7,8 };//其中空白的地方补0
	//int arr[3][4] = { {1,2},{3,4},{5,6} };//这种表示则将括号内的数字按
	////照个数分配给行列
	int arr[][4] = { {1,2},{3,4},{5,6} };//不能省略列，但能省略行
	int i = 0;
	int j = 0;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 4; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}