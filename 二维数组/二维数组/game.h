#pragma once
#include<stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 3
#define COL 3

void chushi(char arr[ROW][COL], int row, int col);
void board(char arr[ROW][COL], int row, int col);
void Computermove(char arr[ROW][COL], int row, int col);
void Playrmove(char arr[ROW][COL], int row, int col);
char win(char arr[ROW][COL], int row, int col);

