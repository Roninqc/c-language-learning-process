#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main01(void)
{
	int a = 0;
	scanf("%d", &a);

	printf("十进制：%d\n", a);
	printf("八进制：%o\n", a);
	printf("十六进制：%x\n", a);
	printf("十六进制：%X\n", a);
	return 0;
}
int main02(void)
{
	float pi = 3.14159;
	float r;
	float s;
	float l;

	scanf("%f", &r);
	s = pi * r * r;
	l = 2 * pi * r;

	printf("面积为：%.2f\n", s);
	printf("周长为：%.2f\n", l);
	return 0;
}
int main(void)
{
	float a = 1.999;
	int b;
	b = (int)a;
	printf("%d\n", b);
	return 0;
}