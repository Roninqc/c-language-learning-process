#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main03()
{
	int i = 0;
	for (i = 1;i <= 10; i++)//使用continue关键字的时候直接进入到执行流程中去
	{
		printf("%d ", i);
	}//循环内部改变循环变量是不可取的
	return 0;
}
int main04()
{
	int i = 0;
	int j = 0;
	for (i = 0; i <3; i++)//当省略最前面的初始值的时候可能会导致后面再进入循环时，初始值并非时自己想要的值，故慎重选择省略。
	{
		for (j = 0;j < 3; j++)
		{
			printf("hehe\n");
		}
	}

}
int main05()
{
	int x, y;
	for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)
	{
		printf("hehe\n");
	}
	return 0;
}

