#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main01()
{
	char password[20] = { 0 };
	printf("请输入密码：");
	scanf("%d", password);//读取字符串的时候读到空格则不从缓冲区再取值了
	printf("请确认密码:");
	//getchar();//有时候如果输入的密码过多则可能导致缓冲区中内容冗杂而导致\n使得程序运行失败。下面是解决方法：
	int  temp = 0;
	while ((temp = getchar()) != '\n')//此为多种输入时避免输入错误的方法。
	{
		;
	}
	int ch = getchar();
	if (ch == 'Y')
		printf("确认成功");
	else
		printf("确认失败");
	return 0;
}

int main02()
{
	int ch = 0;
	while ((ch = getchar()) != EOF)
	{
		if (ch < '0' || ch>'9')//带单引号的字符返回值时ASCII值，由于ASCII值有0到9，所以该语句表达的是数字以外的输入值会导致循环继续
			continue;
		putchar(ch);
	}
	return 0;
}
