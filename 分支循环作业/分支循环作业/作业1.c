#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
int function(int a)
{
	int b;
	switch (a)
	{
	case 1:b = 20;
	case 2:b = 16;
	case 3:b = 30;
	default:b = 0;
	}
	return b;
}
int main1()
{
	printf("%d\n", function(1));
	return 0;
}
int main2()//按照从大到小顺序排列输入的数值
{
	int x=0,y=0,z=0;
	int r = 0;
	scanf("%d", &x);
	scanf("%d", &y);
	scanf("%d", &z);
	if (x < y)
	{
		r = x;
		x = y;
		y = r;
	}
	if (x < z)
	{
		r = x;
		x = z;
		z = r;
	}
	if (y < z)
	{
		r = y;
		y = z;
		z = r;
	}
	printf("%d %d %d", x, y, z);
	return 0;
}
int main3()//求0-100内3的倍数
{
	int i = 0;
	for (i = 1; i <= 100; i++)
	{
		if (i % 3 == 0)
		{
			printf("%d\n", i);
		}
	}
	return 0;
}
int main4()//求最大公约数(普通方法)
{
	int m = 0;
	int n = 0;
	scanf("%d", &m);
	scanf("%d", &n);
	int max = m > n ? m : n;
	while (1)
	{
		if (m % max == 0 && n % max == 0)
		{
			printf("%d\n", max);
			break;
		}
		max--;
	}

	return 0;
}
int main5()//辗转相除法,不需要考虑两数之中的大小。
{
	int m = 0;
	int n = 0;
	scanf("%d", &m);
	scanf("%d", &n);
	int t = 0;
	while (m % n)
	{
		t = m;
		m = n;
		n = t;
		printf("%d", t);
	}
	return 0;
}
int main6()//求最小公倍数(普通方法)
{
	int m = 0;
	int n = 0;
	scanf("%d", &m);
	scanf("%d", &n);
	int max = m > n ? m : n;
	while (1)
	{
		if (max % m == 0 && max % n == 0)
		{
			printf("%d", max);
			break;
		}
		max++;
	}
	return 0;
}