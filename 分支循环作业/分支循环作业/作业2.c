#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <math.h>
int main7()//判断平年闰年
{
	int y = 0;
	for (y = 1000; y <= 2000; y++)
	{
		if (y % 4 == 0)
		{
			if (y % 100 != 0)
			{
				printf("此为闰年：%d\n", y);
			}
		}
		if (y % 400 == 0)
		{
			printf("此为闰年：%d\n", y);
		}
	}
	return 0;
}
int main8()//上述代码变式
{
	int y = 0;
	for (y = 1000; y <= 2000; y++)
	{
		if (y % 4 == 0 && y % 100 != 0 || y % 400 == 0)
		{
			printf("是闰年。%d", y);
		}
	}
	return 0;
}
int main9()
{
	int y = 0;
	int cont = 0;
	for (y = 100; y <= 200; y++)
	{
		int j = 0;
		for (j = 2; j < y; j++)
		{
			if (y % j == 0)
			{
				break;
			}
		}
		if (y == j)
		{
			cont++;
			printf("%d  ", y);
		}

	}
	printf("\n%d", cont);
	return 0;
}
int main10()//上面代码优化
{
	int y = 0;//求素数时由于m=a*B中后面两数其中必有一个小于等于根号m。此时就可
	//以将试验次数减少
	int cont = 0;
	for (y = 101; y <= 200; y+=2)
	{
		int j = 0;
		for (j = 2; j < sqrt(y); j++)
		{
			if (y % j == 0)
			{
				break;
			}
		}
		if (y == j)
		{
			cont++;
			printf("%d  ", y);
		}

	}
	printf("\n%d", cont);
	return 0;
}
